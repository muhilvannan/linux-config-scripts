#!/bin/bash
mkdir /etc/nginx/ssl
mkdir /etc/nginx/ssl/bundles
mkdir /etc/nginx/ssl/certs
mkdir /etc/nginx/ssl/keys
mkdir /etc/nginx/ssl/csr
chmod 644 /etc/nginx/ssl/bundles
chmod 644 /etc/nginx/ssl/certs
chmod 600 /etc/nginx/ssl/keys
chmod 644 /etc/nginx/ssl/csr

echo 'SSL folders configured ....'