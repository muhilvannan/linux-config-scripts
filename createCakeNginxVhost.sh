#!/bin/bash

read -p "Enter the existing username to which you want to add a virtual host: " username
egrep "^$username:" /etc/passwd >/dev/null
        if [ $? -eq 0 ]; then
        echo "awesome the $username exists!"

	read -p "Enter domain to be added without www : " vhostName

echo " 
server {
    listen   80;
    server_name $vhostName;

    root   /home/$username/public_html/webroot/;
    index  index.php;

    access_log /home/$username/logs/access.log;
    error_log /home/$username/logs/error.log;

    location / {
        try_files \$uri \$uri/ /index.php?\$args;
    }

    location ~ \.php\$ {
        root           /home/$username/public_html/webroot;

        fastcgi_buffers 8 32k;
        fastcgi_buffer_size 64k;

        fastcgi_connect_timeout 600;
        fastcgi_send_timeout 600;
        fastcgi_read_timeout 600;

        fastcgi_pass   127.0.0.1:9000;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  \$document_root\$fastcgi_script_name;
        include        fastcgi_params;
    }

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    location ~ /\.ht {
        deny  all;
        access_log      off;
        log_not_found   off;

        }

}
" > /etc/nginx/conf.d/vhost-$username.conf

echo 'created virtual host'

    service nginx reload
    echo "nginx reloaded"

echo 'Website config created. Live long and prosper'
else
		echo "please add user first"
		exit 1
	fi
